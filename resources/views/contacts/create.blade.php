@extends('layouts.app')

@section('title', 'Add Contact')

@section('content')
<h1>Add Contact</h1>

<form action="{{ route('contacts.store') }}" method="post">
    @csrf

    <div class="mb-3">
        <label for="first_name" class="form-label">First Name</label>
        <input type="text" name="first_name" id="first_name" class="form-control">
    </div>

    <div class="mb-3">
        <label for="surname" class="form-label">Surname</label>
        <input type="text" name="surname" id="surname" class="form-control">
    </div>

    <div class="mb-3">
        <label for="phone" class="form-label">Phone</label>
        <input type="text" name="phone" id="phone" class="form-control">
    </div>

    <input type="submit" value="Add" class="btn btn-primary">
</form>
@endsection