@extends('layouts.app')

@section('title', 'Edit Contact')

@section('content')
<h1>Edit Contact</h1>

<form action="{{ route('contacts.update', $contact->id) }}" method="post">
    @csrf
    @method('put')

    <div class="mb-3">
        <label for="first_name" class="form-label">First Name</label>
        <input type="text" name="first_name" id="first_name" class="form-control" value="{{ $contact->first_name }}">
    </div>

    <div class="mb-3">
        <label for="surname" class="form-label">Surname</label>
        <input type="text" name="surname" id="surname" class="form-control" value="{{ $contact->surname }}">
    </div>

    <div class="mb-3">
        <label for="phone" class="form-label">Phone</label>
        <input type="text" name="phone" id="phone" class="form-control" value="{{ $contact->phone }}">
    </div>

    <input type="submit" value="Update" class="btn btn-primary">
</form>
@endsection