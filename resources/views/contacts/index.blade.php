@extends('layouts.app')

@section('title', 'Contacts')

@section('content')

<h1>Contacts</h1>

<p>
    <a href="{{ route('contacts.create') }}" class="btn btn-primary">
        Add a Contact
    </a>
</p>

@if ($contacts->isNotEmpty())
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Surname</th>
            <th>Phone</th>
            <th></th>
        </tr>
    </thead>
    @foreach ($contacts as $contact)
    <tr>
        <td>{{ $contact->first_name }}</td>
        <td>{{ $contact->surname }}</td>
        <td>{{ $contact->phone }}</td>
        <td>
            <a href="{{ route('contacts.edit', $contact->id) }}" class="btn btn-warning btn-sm">
                Edit
            </a>
        </td>
        <td>
            <form action="{{ route('contacts.destroy', $contact->id) }}" method="post">
                @csrf
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </td>
    </tr>
    @endforeach
</table>
@endif

@endsection